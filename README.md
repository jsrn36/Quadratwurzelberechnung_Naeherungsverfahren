Der Projektname ist: Quadratwurzelberechnung_Naeherungsverfahren

Bei diesem Programm geht es darum die Quadratwurzel aus einer Zahl zu berechnen.
Um dies zu überprüfen, wird die Funktion sqrt() für die Zahlen 2 udn 16 verwendet.

Das Vorgehen wird so sein, dass eine Formel immer und immer wieder aufgerufen wird, bis das Ergebnis dieser
Berechnung mit dem der sqrt() Funktion übereinstimmt.

Die größte Herausforderung dieser Aufgabe wird sein die Zahl bis auf eine bestimmt Nachkommazahl zu vergleichen.
Bei 16 wäre dies nicht so schwer allerdings bei der Zahl 2 sind die Nachkommastellen unendlich, daher wird man 
bis auf eine bestimmt Anzahl an Nachkommastellen vergleichen müssen.

Als Lösungsansatz würde ich eine Funktion schreiben die der Näherungsformel entspricht und diese Rekursiv so 
lange aufrufen bis z. B. die ersten 8 Nachkommastellen mit der Zahl der sqrt() Funktion übereinstimmen.