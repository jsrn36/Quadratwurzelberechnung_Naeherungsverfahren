//
//  main.c
//  Naeherungsverfahren_Quadratwurzel
//
//  Created by Jannik Sörensen on 06.01.16.
//  Copyright © 2016 Jannik Sörensen. All rights reserved.
//

#include <stdio.h>
#include <math.h>

int durchlauf = 0;
double zahl;
double zahl2;
double ergebnis;
double startwert = 1.0;

// Version Nr. 1

double heron(double zahl){
    ergebnis = 0.5*(startwert+(zahl/startwert));
    if(ergebnis != startwert){
        startwert = ergebnis;
        heron (zahl);
    }
    return (ergebnis);
}

// Version Nr. 2
/*
double heron(double zahl){
    int i;
    for(i=0;i<100;i++){
        ergebnis = 0.5*(startwert+(zahl/startwert));
        durchlauf++;
        if(ergebnis == startwert){
            break;
        }
        else{
            startwert = ergebnis;
        }
    }
    printf("Es wurden %d Durchläufe und das ", durchlauf);
    return(ergebnis);
}*/

int main(int argc, const char * argv[]) {
    printf("Aus welcher Zahl soll die Wurzel gezogen werden? \n");
    scanf("%lg",&zahl);
    printf("Gib eine zweite Zahl zur Wurzelberechnung ein \n");
    scanf("%lg",&zahl2);
    heron(zahl);
    printf("Ergebnis der Heron-Funktion für die erste Zahl: %lg \n",ergebnis);
    heron(zahl2);
    printf("Ergebnis der Heron-Funktion für die zweite Zahl: %lg \n",ergebnis);
    printf("Wurzel aus 2: %lg \n",sqrt(2));
    printf("Wurzel aus 16: %lg \n",sqrt(16));
    return 0;
}
